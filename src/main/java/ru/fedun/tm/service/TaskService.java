package ru.fedun.tm.service;

import ru.fedun.tm.api.repository.ICrudRepository;
import ru.fedun.tm.api.service.ICrudService;
import ru.fedun.tm.exception.empty.EmptyIdException;
import ru.fedun.tm.exception.empty.EmptyTitleException;
import ru.fedun.tm.exception.empty.EmptyUserIdException;
import ru.fedun.tm.exception.incorrect.IncorrectIndexException;
import ru.fedun.tm.exception.notfound.TaskNotFoundException;
import ru.fedun.tm.entity.Task;

import java.util.List;

public class TaskService implements ICrudService<Task> {

    private final ICrudRepository<Task> taskRepository;

    public TaskService(final ICrudRepository<Task> taskRepository) {
        this.taskRepository = taskRepository;
    }

    public List<Task> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return taskRepository.findAll(userId);
    }

    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        taskRepository.clear(userId);
    }

    @Override
    public void create(final String userId, final String title) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (title == null || title.isEmpty()) throw new EmptyTitleException();
        final Task task = new Task();
        task.setTitle(title);
        taskRepository.add(userId, task);
    }

    @Override
    public void create(final String userId, final String title, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (title == null || title.isEmpty()) throw new EmptyTitleException();
        final Task task = new Task();
        task.setTitle(title);
        task.setDescription(description);
        taskRepository.add(userId, task);
    }

    @Override
    public Task getOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyTitleException();
        return taskRepository.findOneById(userId, id);
    }

    @Override
    public Task getOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return taskRepository.findOneByIndex(userId, index);
    }

    @Override
    public Task getOneByTitle(final String userId, final String title) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (title == null || title.isEmpty()) throw new EmptyTitleException();
        return taskRepository.findOneByTitle(userId, title);
    }

    @Override
    public Task updateById(final String userId, final String id, final String title, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (title == null || title.isEmpty()) throw new EmptyTitleException();
        final Task task = getOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setId(id);
        task.setTitle(title);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final String userId, final Integer index, final String title, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (title == null || title.isEmpty()) throw new EmptyTitleException();
        final Task task = getOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setTitle(title);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task removeOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.removeOneById(userId, id);
    }

    @Override
    public Task removeOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return taskRepository.removeOneByIndex(userId, index);
    }

    @Override
    public Task removeOneByTitle(final String userId, final String title) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (title == null || title.isEmpty()) throw new EmptyTitleException();
        return taskRepository.removeOneByTitle(userId, title);
    }

}
