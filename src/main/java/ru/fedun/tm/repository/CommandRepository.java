package ru.fedun.tm.repository;

import ru.fedun.tm.api.repository.ICommandRepository;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.command.auth.LoginCommand;
import ru.fedun.tm.command.auth.LogoutCommand;
import ru.fedun.tm.command.auth.RegistrationCommand;
import ru.fedun.tm.command.project.ProjectClearCommand;
import ru.fedun.tm.command.project.ProjectCreateCommand;
import ru.fedun.tm.command.project.remove.ProjectRemoveByIdCommand;
import ru.fedun.tm.command.project.remove.ProjectRemoveByIndexCommand;
import ru.fedun.tm.command.project.remove.ProjectRemoveByTitleCommand;
import ru.fedun.tm.command.project.show.ProjectShowAllCommand;
import ru.fedun.tm.command.project.show.ProjectShowByIdCommand;
import ru.fedun.tm.command.project.show.ProjectShowByIndexCommand;
import ru.fedun.tm.command.project.show.ProjectShowByTitleCommand;
import ru.fedun.tm.command.project.update.ProjectUpdateByIdCommand;
import ru.fedun.tm.command.project.update.ProjectUpdateByIndexCommand;
import ru.fedun.tm.command.system.*;
import ru.fedun.tm.command.task.TaskClearCommand;
import ru.fedun.tm.command.task.TaskCreateCommand;
import ru.fedun.tm.command.task.remove.TaskRemoveByIdCommand;
import ru.fedun.tm.command.task.remove.TaskRemoveByIndexCommand;
import ru.fedun.tm.command.task.remove.TaskRemoveByTitleCommand;
import ru.fedun.tm.command.task.show.TaskShowAllCommand;
import ru.fedun.tm.command.task.show.TaskShowByIdCommand;
import ru.fedun.tm.command.task.show.TaskShowByIndexCommand;
import ru.fedun.tm.command.task.show.TaskShowByTitleCommand;
import ru.fedun.tm.command.task.update.TaskUpdateByIdCommand;
import ru.fedun.tm.command.task.update.TaskUpdateByIndexCommand;
import ru.fedun.tm.command.user.EmailChangeCommand;
import ru.fedun.tm.command.user.PasswordChangeCommand;
import ru.fedun.tm.command.user.ProfileShowCommand;

import java.util.ArrayList;
import java.util.List;

public class CommandRepository implements ICommandRepository {

    private final List<AbstractCommand> commandList = new ArrayList<>();

    {
        commandList.add(new LoginCommand());
        commandList.add(new LogoutCommand());
        commandList.add(new RegistrationCommand());
        commandList.add(new ProfileShowCommand());
        commandList.add(new EmailChangeCommand());
        commandList.add(new PasswordChangeCommand());
        commandList.add(new TaskCreateCommand());
        commandList.add(new TaskClearCommand());
        commandList.add(new TaskShowAllCommand());
        commandList.add(new TaskShowByIdCommand());
        commandList.add(new TaskShowByTitleCommand());
        commandList.add(new TaskShowByIndexCommand());
        commandList.add(new TaskRemoveByIdCommand());
        commandList.add(new TaskRemoveByIndexCommand());
        commandList.add(new TaskRemoveByTitleCommand());
        commandList.add(new TaskUpdateByIdCommand());
        commandList.add(new TaskUpdateByIndexCommand());
        commandList.add(new ProjectCreateCommand());
        commandList.add(new ProjectClearCommand());
        commandList.add(new ProjectShowAllCommand());
        commandList.add(new ProjectShowByIdCommand());
        commandList.add(new ProjectUpdateByIndexCommand());
        commandList.add(new ProjectShowByTitleCommand());
        commandList.add(new ProjectShowByIndexCommand());
        commandList.add(new ProjectRemoveByIdCommand());
        commandList.add(new ProjectRemoveByIndexCommand());
        commandList.add(new ProjectRemoveByTitleCommand());
        commandList.add(new ProjectUpdateByIdCommand());
        commandList.add(new HelpCommand());
        commandList.add(new AboutCommand());
        commandList.add(new SystemInfoCommand());
        commandList.add(new VersionCommand());
        commandList.add(new CommandsViewCommand());
        commandList.add(new ArgumentsViewCommand());
        commandList.add(new ExitCommand());
    }

    public List<AbstractCommand> getCommands() {
        return commandList;
    }

}
