package ru.fedun.tm.api.service;

import ru.fedun.tm.entity.User;
import ru.fedun.tm.role.Role;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User create(String login, String password, String firstName, String lastName);

    User create(String login, String password, String firstName, String lastName, String email);

    User create(String login, String password, String firstName, String lastName, Role role);

    User findById(String id);

    User findByLogin(String login);

    User updateMail(String userId, String email);

    User updatePassword(String userId, String password, String newPassword);

    User removeById(String id);

    User removeByLogin(String login);

    User removeUser(String userId, User user);

}
