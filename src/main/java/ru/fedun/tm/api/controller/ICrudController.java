package ru.fedun.tm.api.controller;

public interface ICrudController<T> {

    void showAll();

    void clear();

    void create();

    void showOneById();

    void showOneByIndex();

    void showOneByTitle();

    void updateOneById();

    void updateOneByIndex();

    void removeOneById();

    void removeOneByIndex();

    void removeOneByTitle();

}
