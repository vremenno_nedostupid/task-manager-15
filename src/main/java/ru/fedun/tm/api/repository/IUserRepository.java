package ru.fedun.tm.api.repository;

import ru.fedun.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    User add(User user);

    User remove(User user);

    List<User> findAll();

    User findById(String id);

    User findByLogin(String login);

    User removeById(String id);

    User removeByLogin(String login);

}
