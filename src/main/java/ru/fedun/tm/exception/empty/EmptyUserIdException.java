package ru.fedun.tm.exception.empty;

import ru.fedun.tm.exception.AbstractRuntimeException;

public class EmptyUserIdException extends AbstractRuntimeException {

    private final static String message = "Error! User ID is empty...";

    public EmptyUserIdException() {
        super(message);
    }
}
