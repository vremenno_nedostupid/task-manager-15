package ru.fedun.tm.exception.empty;

import ru.fedun.tm.exception.AbstractRuntimeException;

public final class EmptyIdException extends AbstractRuntimeException {

    private static final String message = "Error! Id is empty...";

    public EmptyIdException() {
        super(message);
    }

}
