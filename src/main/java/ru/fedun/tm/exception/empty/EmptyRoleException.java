package ru.fedun.tm.exception.empty;

import ru.fedun.tm.exception.AbstractRuntimeException;

public class EmptyRoleException extends AbstractRuntimeException {

    private final static String message = "Error! Role is empty...";

    public EmptyRoleException() {
        super(message);
    }

}
