package ru.fedun.tm.exception.user;

import ru.fedun.tm.exception.AbstractRuntimeException;

public class AccessDeniedException extends AbstractRuntimeException {

    private final static String message = "Error! Access denied...";

    public AccessDeniedException() {
        super(message);
    }
}
