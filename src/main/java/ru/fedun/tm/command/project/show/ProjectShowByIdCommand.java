package ru.fedun.tm.command.project.show;

import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.entity.Project;
import ru.fedun.tm.entity.Task;
import ru.fedun.tm.exception.user.AccessDeniedException;
import ru.fedun.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-view-by-id";
    }

    @Override
    public String description() {
        return "Show project by id.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().getOneById(userId, id);
        if (project == null) throw new AccessDeniedException();
        System.out.println("ID: " + project.getId());
        System.out.println("TITLE: " + project.getTitle());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
    }

}
