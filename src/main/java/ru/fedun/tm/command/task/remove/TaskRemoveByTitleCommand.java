package ru.fedun.tm.command.task.remove;

import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.entity.Task;
import ru.fedun.tm.util.TerminalUtil;

public final class TaskRemoveByTitleCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-remove-by-title";
    }

    @Override
    public String description() {
        return "Remove task by title.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER TITLE:]");
        final String title = TerminalUtil.nextLine();
        if (title == null || title.isEmpty()) {
            System.out.println("[FAIL]");
            return;
        }
        final Task task = serviceLocator.getTaskService().removeOneByTitle(userId, title);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        System.out.println();
    }

}
