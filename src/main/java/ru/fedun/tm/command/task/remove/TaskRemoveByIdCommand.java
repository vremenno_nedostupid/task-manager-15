package ru.fedun.tm.command.task.remove;

import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.entity.Task;
import ru.fedun.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-remove-by_id";
    }

    @Override
    public String description() {
        return "Remove task by id.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        if (id == null || id.isEmpty()) {
            System.out.println("[FAIL]");
            return;
        }
        final Task task = serviceLocator.getTaskService().removeOneById(userId, id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        System.out.println();
    }

}
