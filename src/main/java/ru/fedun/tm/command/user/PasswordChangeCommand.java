package ru.fedun.tm.command.user;

import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.util.TerminalUtil;

public class PasswordChangeCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "change-password";
    }

    @Override
    public String description() {
        return "Change profile password.";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER OLD PASSWORD:]");
        final String oldPassword = TerminalUtil.nextLine();
        System.out.println("[ENTER NEW PASSWORD:]");
        final String newPassword = TerminalUtil.nextLine();
        serviceLocator.getUserService().updatePassword(userId, oldPassword, newPassword);
        System.out.println("[OK]");
        System.out.println();
    }
}
