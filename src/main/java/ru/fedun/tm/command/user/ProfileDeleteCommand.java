package ru.fedun.tm.command.user;

import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.entity.User;
import ru.fedun.tm.util.TerminalUtil;

public class ProfileDeleteCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "delete-profile";
    }

    @Override
    public String description() {
        return "Delete profile.";
    }

    @Override
    public void execute() {
        System.out.println("[DELETE PROFILE]");
        System.out.println("DO YOU REALLY WANT DELETE PROFILE?");
        System.out.println("1 - YES, 2 - NO");
        final Integer answer = TerminalUtil.nextInt();
        if (answer.equals(1)) {
            final String userId = serviceLocator.getAuthService().getUserId();
            final User user = serviceLocator.getUserService().findById(userId);
            serviceLocator.getUserService().removeUser(userId, user);
        } else return;
        System.out.println("[OK]");
        System.out.println();
    }

}
